FROM ubuntu:latest

RUN apt-get update && \
     apt-get install -y \
        wget \
        sudo \
        git \
        gcc \
        curl \
        g++

RUN sudo -E bash nheq.sh

CMD [nheq.sh]
